package com.example.neural.network;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class NeuralNetworkApplicationTests {

	@Test
	@Disabled
	void contextLoads() {
	}

}
