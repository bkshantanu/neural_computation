package com.example.neural.network.controller.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Model {
    private List<Integer> inputs;
    private List<Integer> weights = new ArrayList<>();
    private List<Integer> bias = new ArrayList<>();
    private String compute = "SUM"; // Sin, Log
}
