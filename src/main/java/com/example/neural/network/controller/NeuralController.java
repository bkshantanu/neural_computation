package com.example.neural.network.controller;

import com.example.neural.network.controller.model.Model;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

@RestController
public class NeuralController {

    @PostMapping(value = "/neuron", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getComputedByNeuron(@RequestBody Model model) {
        //validation if inputs, weights, bias size not matching
        if (CollectionUtils.isEmpty(model.getInputs())) {
            return handleConstraintViolationException(new ConstraintViolationException("inputs attribute is mandatory", null));
        }
        long summation = 0;
        //Summation of {(intput * weights) + bias}
        if (!StringUtils.hasText(model.getCompute()) || "SUM".equalsIgnoreCase(model.getCompute())) {
            int weightSize = CollectionUtils.isEmpty(model.getWeights()) ? 0 : model.getWeights().size();
            int biasSize = CollectionUtils.isEmpty(model.getBias()) ? 0 : model.getBias().size();
            int weight = 0;
            int bias = 0;
            int input = 0;
            for (int i = 0; i < model.getInputs().size(); i++) {
                weight = i <= weightSize - 1 ? model.getWeights().get(i) : 1;
                bias = i <= biasSize - 1 ? model.getBias().get(i) : 0;
                input = model.getInputs().get(i);
                summation += ((input * weight) + bias);
            }
        }

        return new ResponseEntity<>(summation, HttpStatus.OK);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
