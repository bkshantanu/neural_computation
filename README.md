# README #
Neural Network is a Java Spring-boot application to handle SUM, Log, Sin of the inputs 
and returns the calulated output in long.

The attribute 'inputs' is mandatory. Other empty attributes defaulted as below.
    
    default compute -> SUM
    default weights -> 1
    default bias -> 0

### Sample Input ###
```
{
    "inputs": [1,2,3],
    "weights": [1,2,3],
    "bias": [1,1],
    "compute": "SUM"
}
```

### Sample Response ###
`16`

### Setup ###
1. `git clone https://bkshantanu@bitbucket.org/bkshantanu/neural_computation.git`
2. `cd <Project_Directory>`
3. Compile:`mvn clean install`
4. Run: `mvn spring-boot:run`
5. Test with any API tool with http POST using url: http://localhost:8080/neuron



